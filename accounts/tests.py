from django.test import TestCase

# Create your tests here.
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIRequestFactory
from django.contrib.auth.models import User
from rest_framework import status


class AccountsTest(APITestCase):
    username = "testuser"
    password = "testpassword"

    def setUp(self):
        self.user = User.objects.create_user(
            username=self.username,
            password=self.password
        )

    def test_create_user(self):
        response = self.client.post('/api/token/', {
            'username': self.username,
            'password': self.password
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
