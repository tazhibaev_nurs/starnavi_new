import datetime
from django.contrib.auth.models import AbstractUser, User
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db import models
from simple_history.models import HistoricalRecords


class Post(models.Model):
    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author_posts',
                               verbose_name='Автор поста')
    title = models.CharField('Заголовок поста', max_length=64)
    description = models.TextField('Описание поста', null=True)
    image = models.ImageField('Фото поста', upload_to='media/')
    like = models.ManyToManyField(User, related_name='posts', blank=True)
    created = models.DateField(verbose_name='Дата создания', auto_now_add=datetime.date.today, null=True,
                               blank=True)
    updated = models.DateField(verbose_name='Дата обновления', auto_now_add=datetime.date.today,
                               null=True, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return f"{self.title}"

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value
