from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from accounts.models import Post


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')

    email = serializers.EmailField(required=True, max_length=32,
                                   validators=[UniqueValidator(queryset=User.objects.all())])
    username = serializers.CharField(validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])
        return user


class PostSerializer(serializers.ModelSerializer):
    author_name = serializers.SerializerMethodField('get_author_name')
    total_like = serializers.SerializerMethodField('get_total_like')

    class Meta:
        model = Post
        fields = ('id', 'author_name', 'title', 'created', 'like', 'total_like')

    def get_author_name(self, obj):
        if obj.author:
            return obj.author.username
        return None

    def get_total_like(self, obj):
        if obj.like:
            return obj.like.count()
        return None


class PostDetailSerializer(serializers.ModelSerializer):
    author_name = serializers.SerializerMethodField('get_author_name')

    class Meta:
        model = Post
        fields = ('id', 'author_name', 'title', 'description', 'image', 'created', 'updated', 'like')

    def get_author_name(self, obj):
        if obj.author:
            return obj.author.username
        return None

    def create(self, validated_data):
        return Post(**validated_data)
