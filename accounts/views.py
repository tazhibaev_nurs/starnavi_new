from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, filters
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_simplejwt import authentication

from accounts.models import Post
from accounts.serializers import UserSerializer, PostSerializer, PostDetailSerializer


class UserView(IsAuthenticated, ModelViewSet):
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = (authentication.JWTAuthentication,)
    lookup_field = 'pk'

    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, *args, **kwargs):
        instance = User.objects.get(pk=kwargs['pk'])
        serializer = UserSerializer(instance, context=self.get_serializer_context())
        return Response(serializer.data)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request
        return context

    def update(self, request, *args, **kwargs):

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()


class PostView(IsAuthenticated, ModelViewSet):
    model = Post
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_field = 'pk'

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = PostDetailSerializer(instance, context=self.get_serializer_context())
        return Response(serializer.data)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request
        return context

    def create(self, request, *args, **kwargs):

        data = {
            'author_name': request.user,
            'title': request.data.get('title'),
            'description': request.data.get('description'),
            'image': request.data.get('image'),
        }
        serializer = PostDetailSerializer(data=data)
        if serializer.is_valid():
            post = serializer.save()
            if post:
                post.author = request.user
                post.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LikeView(IsAuthenticated, ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_field = 'pk'

    def create(self, request, *args, **kwargs):
        user = request.user
        author_posts = user
        post_id = request.data['post_id']
        is_like = request.data['is_like']
        post = Post.objects.get(pk=post_id)
        if is_like == True:
            if post not in author_posts.posts.all():
                instance = author_posts.posts.add(post)
                return Response(
                    {'message': f'You are liked the post {post_id}'}, status=status.HTTP_200_OK
                )
            elif post in author_posts.posts.all():
                return Response(
                    {'message': 'You have already liked this post', 'error_code': '409'},
                    status=status.HTTP_409_CONFLICT)
        else:
            if post in author_posts.posts.all():
                instance = author_posts.posts.remove(post)
                return Response(
                    {'message': f'You unliked this post {post_id}', },
                    status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        user = request.user
        author_posts = user
        post = author_posts.posts.all()
        try:
            liked_list = []
            for item in post:
                liked_list.append(item)
            serializer = PostDetailSerializer(liked_list, many=True, context={'request': request, 'user': user})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response({'message': 'Not Found', 'error_code': '404'}, status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        user = request.user
        author_posts = user
        author_posts.posts.clear()
        return Response({'message': 'All likes are unliked from all posts'}, status=status.HTTP_204_NO_CONTENT)


class LikeCountView(IsAuthenticated, ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_field = 'pk'
    search_fields = ['title', 'created']
    filter_backends = [filters.OrderingFilter, SearchFilter]

    def get_queryset(self):
        queryset = Post.objects.all()
        order = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('search'):
            queryset = queryset.filter(title__icontains=self.request.GET.get('search'))

        if self.request.GET.get('created_from'):
            queryset = queryset.filter(created__gte=self.request.GET.get('created_from'))
        if self.request.GET.get('created_to'):
            queryset = queryset.filter(created__lte=self.request.GET.get('created_to'))

        if filter_fields:
            queryset = queryset.filter(**filter_fields)
        return queryset.order_by('id')
